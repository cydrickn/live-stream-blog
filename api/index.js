const express = require('express')
const axios = require('axios')
const router = express.Router()
const config = require('./config')

router.get('/posts', function(req, res) {
  const projectID = config.id
  axios
    .get(`https://gitlab.com/api/v4/projects/${projectID}/issues`)
    .then((result) => {
      res.send(result.data)
    })
    .catch((err) => {
      console.log(err)
    })
})

router.get('/posts/:slug', function(req, res) {
  const params = req.params
  const projectID = config.id
  axios
    .get(`https://gitlab.com/api/v4/projects/${projectID}/issues?search=${params.slug}`)
    .then((result) => {
      res.send(result.data[0])
    })
    .catch((err) => {
      console.log(err)
    })
})

export default {
  path: '/api',
  handler: router
}
